package com.ingrails.primecollege.broadcastreceiver.localbroadcastreceiver;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

public class Sender{
    private Context context;

    Sender(Context context) {
        this.context = context;
        getNumberType(2);
    }

    private void getNumberType(int number) {
        String type = number % 2 == 0 ? "even" : "odd";
        sendMessage(type);
    }

    private void sendMessage(String numberType) {
        Intent intent = new Intent("1000");
        intent.putExtra("message", numberType);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

}
