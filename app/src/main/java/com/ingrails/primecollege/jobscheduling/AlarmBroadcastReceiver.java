package com.ingrails.primecollege.jobscheduling;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.widget.Toast;

import com.ingrails.primecollege.R;

/**
 * Created by gokarna on 11/5/17.
 * alarm broadcast receiver
 */

public class AlarmBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.alarmclock);
        mediaPlayer.start();
        Toast.makeText(context, "Alarm....", Toast.LENGTH_LONG).show();
    }
}
