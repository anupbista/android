package com.ingrails.primecollege.jobscheduling.firebasejobdispatcher;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.Trigger;
import com.ingrails.primecollege.R;

public class FirebaseJobActivity extends AppCompatActivity {
    //https://enyclopedi2a@bitbucket.org/enyclopedi2a/prime.git
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase_job);
        FirebaseJobDispatcher dispatcher =
                new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(FirebaseJobService.class)
                .setRecurring(false)
                .setTrigger(Trigger.executionWindow(0, 3))
                .setLifetime(Lifetime.FOREVER)
                .setReplaceCurrent(true)
                .setConstraints(Constraint.ON_UNMETERED_NETWORK, Constraint.DEVICE_CHARGING)
                .setTag("my-unique-tag")
                .build();
        dispatcher.mustSchedule(myJob);
    }
}
