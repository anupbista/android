package com.ingrails.primecollege.jobscheduling.firebasejobdispatcher;

import android.media.MediaPlayer;
import android.widget.Toast;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.ingrails.primecollege.R;

/**
 * Created by gokarna on 11/5/17.
 * job service
 */

public class FirebaseJobService extends JobService {

    @Override
    public boolean onStartJob(JobParameters job) {
        MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.alarmclock);
        mediaPlayer.start();
        Toast.makeText(getApplicationContext(), "Alarm....", Toast.LENGTH_LONG).show();
        return false;// Answers the question: "Is there still work going on?"
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;// Answers the question: "Should this job be retried?"
    }
}
