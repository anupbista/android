package com.ingrails.primecollege.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ingrails.primecollege.R;
import com.ingrails.primecollege.apiservices.ApiClient;
import com.ingrails.primecollege.models.Login;
import com.ingrails.primecollege.services.EventNepalCallback;
import com.ingrails.primecollege.services.LoginApiService;
import com.ingrails.primecollege.services.SignUpApiService;
import com.ingrails.primecollege.utils.PrimeCollegeApplication;
import com.ingrails.primecollege.utils.Utilities;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by gokarna on 11/26/17.
 * tab layout fragment
 */

public class RestfulApiFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_layout_view, container, false);
        signUp("Ram", "ram.adhikari@gmail.com", "2017-09-23", "ramAdhikari", "ramAdhikari", "Male");
        return view;
    }

    private void signUp(String name, String email, String dateOfBirth, String password, String confirmPassword, String gender) {
        SignUpApiService signUpApiService = ApiClient.getClient().create(SignUpApiService.class);
        signUpApiService.registerUser(name, email, password, confirmPassword, dateOfBirth, gender).enqueue(new EventNepalCallback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        Toast.makeText(getActivity(), "Successfully sign up", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable throwable) {
                super.onFailure(call, throwable);
                Toast.makeText(getActivity(), super.getCause(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void login(String email, String password) {
        LoginApiService loginApiService = ApiClient.getClient().create(LoginApiService.class);
        loginApiService.requestLogin(email, password, PrimeCollegeApplication.getSharedPreference().getString("hsdhashdasdasdadjasjdhuhfasufsaudfuasid", "")).enqueue(new EventNepalCallback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        Utilities.saveLoginResponse(response.body());
                    } else {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable throwable) {
                super.onFailure(call, throwable);
                Toast.makeText(getActivity(), super.getCause(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
