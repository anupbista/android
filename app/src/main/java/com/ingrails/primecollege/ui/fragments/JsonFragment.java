package com.ingrails.primecollege.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingrails.primecollege.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Created by gokarna on 11/26/17.
 * tab layout fragment
 */

public class JsonFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        String json = getRawJson();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            String id = jsonObject.getString("id");
            String type = jsonObject.getString("type");
            String name = jsonObject.getString("name");
            Log.e("id", id);
            Log.e("type", type);
            Log.e("name", name);
            JSONObject batters = jsonObject.getJSONObject("batters");
            JSONArray jsonArray = batters.getJSONArray("batter");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String batterId = jsonObject1.getString("id");
                String batterType = jsonObject1.getString("type");
                Log.e("Batter id", batterId);
                Log.e("Batter Type", batterType);
            }
            JSONArray topping = jsonObject.getJSONArray("topping");
            for (int i = 0; i < topping.length(); i++) {
                JSONObject jsonObject1 = topping.getJSONObject(i);
                String toppingId = jsonObject1.getString("id");
                String toppingrType = jsonObject1.getString("type");
                Log.e("Topping Id", toppingId);
                Log.e("Topping Type", toppingrType);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return inflater.inflate(R.layout.tab_layout_view, container, false);
    }

    private String getRawJson() {
        InputStream is = getResources().openRawResource(R.raw.sample);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            is.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return writer.toString();
    }
}
