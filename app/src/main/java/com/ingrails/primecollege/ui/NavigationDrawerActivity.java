package com.ingrails.primecollege.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.ingrails.primecollege.R;
import com.ingrails.primecollege.ui.fragments.AnimationFragment;
import com.ingrails.primecollege.ui.fragments.BottomNavigationFragment;
import com.ingrails.primecollege.ui.fragments.JsonFragment;
import com.ingrails.primecollege.ui.fragments.RecyclerViewFragment;
import com.ingrails.primecollege.ui.fragments.RestfulApiFragment;
import com.ingrails.primecollege.ui.fragments.TabLayoutFragment;

public class NavigationDrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.inflateMenu(R.menu.navigation_drawer_menu);
        prepareDrawer();
        setSupportActionBar(toolbar);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);
        setUpFragment(new RecyclerViewFragment(), "recycler_view");
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sample_menu, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawerLayout.closeDrawer(GravityCompat.START);
        switch (item.getItemId()) {
            case R.id.recycler_view:
                setUpFragment(new RecyclerViewFragment(), getResources().getString(R.string.recycler_view));
                break;
            case R.id.bottom_navigation:
                setUpFragment(new BottomNavigationFragment(), getResources().getString(R.string.bottom_navigation));
                break;
            case R.id.tablayout:
                setUpFragment(new TabLayoutFragment(), getResources().getString(R.string.tablayout));
                break;
            case R.id.json:
                setUpFragment(new RestfulApiFragment(), getResources().getString(R.string.rest_api));
                break;
            case R.id.restful_services:
                setUpFragment(new JsonFragment(), getResources().getString(R.string.json));
                break;
            case R.id.animations:
                setUpFragment(new AnimationFragment(), getResources().getString(R.string.animations));
                break;
        }
        return true;
    }

    /**
     * @param fragment fragment to load
     */
    private void setUpFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment, tag);
        fragmentTransaction.commit();
    }

    public void prepareDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

}
