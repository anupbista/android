package com.ingrails.primecollege.ui.fragments;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.ingrails.primecollege.R;

/**
 * Created by gokarna on 12/15/17.
 * animation fragment
 */

public class AnimationFragment extends Fragment implements View.OnClickListener {
    private ImageView animationView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.animation_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        animationView = view.findViewById(R.id.animationView);
        Button rotation = view.findViewById(R.id.rotation);
        Button translation = view.findViewById(R.id.translation);
        Button scaling = view.findViewById(R.id.scaling);
        Button alpha = view.findViewById(R.id.alpha);
        rotation.setOnClickListener(this);
        translation.setOnClickListener(this);
        scaling.setOnClickListener(this);
        alpha.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        ObjectAnimator animator = null;
        switch (view.getId()) {
            case R.id.rotation:
                animator = ObjectAnimator.ofFloat(animationView, "rotation", 0f, 360f);
                animator.setDuration(1000);
                animator.start();
                break;
            case R.id.translation:
                if (animationView.getTranslationX() == 0f) {
                    animator = ObjectAnimator.ofFloat(animationView, "translationX", 100f);
                    animator.setDuration(1000);
                } else {
                    animator = ObjectAnimator.ofFloat(animationView, "translationX", 0f);
                    animator.setDuration(1000);
                }
                animator.start();
                break;
            case R.id.scaling:
                ObjectAnimator scaleDownX = null;
                ObjectAnimator scaleDownY = null;
                if (animationView.getScaleX() == 0.5) {
                    scaleDownX = ObjectAnimator.ofFloat(animationView, "scaleX", 1f);
                    scaleDownY = ObjectAnimator.ofFloat(animationView, "scaleY", 1f);

                } else {
                    scaleDownX = ObjectAnimator.ofFloat(animationView, "scaleX", 0.5f);
                    scaleDownY = ObjectAnimator.ofFloat(animationView, "scaleY", 0.5f);
                }
                scaleDownX.setDuration(1000);
                scaleDownY.setDuration(1000);
                AnimatorSet scaleDown = new AnimatorSet();
                scaleDown.play(scaleDownX).with(scaleDownY);
                scaleDown.start();
                break;
            case R.id.alpha:
                if (animationView.getAlpha() == 1f) {
                    animator = ObjectAnimator.ofFloat(animationView, "alpha", 1f, 0f).
                            setDuration(1000);
                } else {
                    animator = ObjectAnimator.ofFloat(animationView, "alpha", 0f, 1f).
                            setDuration(1000);
                }
                animator.setDuration(1000);
                animator.start();
                break;
        }
    }
}
