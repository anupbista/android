package com.ingrails.primecollege.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingrails.primecollege.R;
import com.ingrails.primecollege.models.Magazine;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gokarna on 11/18/17.
 * recycler view fragment
 */

public class RecyclerViewFragment extends Fragment {
    private RecyclerView recycler_view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recycler_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recycler_view = view.findViewById(R.id.recycler_view);
        prepareRecyclerView();
    }

    private void prepareRecyclerView() {
        LinearLayoutManager manager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        recycler_view.setLayoutManager(manager);
        recycler_view.setHasFixedSize(true);
        recycler_view.setAdapter(new MagazineAdapter(getDataSource()));
    }

    private List<Magazine> getDataSource() {
        List<Magazine> magazineList = new ArrayList<>();
        int images[] = new int[]{R.mipmap.one, R.mipmap.two, R.mipmap.three, R.mipmap.four, R.mipmap.five, R.mipmap.six
                , R.mipmap.seven, R.mipmap.eight, R.mipmap.nine, R.mipmap.ten};
        String title[] = new String[]{"Enterpreneur Magazine title", "Enterpreneur Magazine title"
                , "Enterpreneur Magazine title", "Enterpreneur Magazine title", "Enterpreneur Magazine title", "Enterpreneur Magazine title", "Enterpreneur Magazine title",
                "Enterpreneur Magazine title", "Enterpreneur Magazine title","Enterpreneur Magazine title"};
        String description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled ";
        for (int i = 0; i < images.length; i++) {
            Magazine magazine = new Magazine();
            magazine.setTitle(title[i]);
            magazine.setDescription(description);
            magazine.setImage(images[i]);
            magazineList.add(magazine);
        }
        return magazineList;
    }


    private class MagazineAdapter extends RecyclerView.Adapter<MagazineAdapter.MagazineViewHolder> {

        private List<Magazine> magazineList;

        MagazineAdapter(List<Magazine> magazineList) {
            this.magazineList = magazineList;
        }

        @Override
        public MagazineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.magazine_list_row, parent, false);
            return new MagazineViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MagazineViewHolder holder, final int position) {
            Magazine magazine = magazineList.get(position);
            holder.image.setImageResource(magazine.getImage());
            holder.title.setText(magazine.getTitle());
            holder.description.setText(magazine.getDescription());
        }

        @Override
        public int getItemCount() {
            return magazineList.size();
        }


        class MagazineViewHolder extends RecyclerView.ViewHolder {
            ImageView image;
            TextView title, description;

            MagazineViewHolder(View itemView) {
                super(itemView);
                title = itemView.findViewById(R.id.title);
                description = itemView.findViewById(R.id.description);
                image = itemView.findViewById(R.id.image);
            }
        }
    }
}
