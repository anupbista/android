package com.ingrails.primecollege.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by gokarna on 3/21/17.
 * return application context for all over the app
 */

public class PrimeCollegeApplication extends Application {
    private static PrimeCollegeApplication primeCollegeApplication;
    private static SharedPreferences sharedPreferences;


    public void onCreate() {
        super.onCreate();
        primeCollegeApplication = this;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getAppContext());
    }

    public static Context getAppContext() {
        return primeCollegeApplication.getApplicationContext();
    }

    public static SharedPreferences getSharedPreference() {
        return sharedPreferences;
    }
}
