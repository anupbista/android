package com.ingrails.primecollege.utils;

import android.content.SharedPreferences;

import com.google.gson.GsonBuilder;
import com.ingrails.primecollege.models.Login;


/**
 * Created by gokarna on 6/11/17.
 * save app response in cache
 */

public class Utilities {
    /**
     * @return Login response
     */
    public static Login getLoginResponse() {
        String savedUserResponse = PrimeCollegeApplication.getSharedPreference().getString(Constants.LOGIN_REPONSE, null);
        return new GsonBuilder().create().fromJson(savedUserResponse, Login.class);
    }

    /**
     * @param login save login response
     */
    public static void saveLoginResponse(Login login) {
        String json = new GsonBuilder().create().toJson(login);
        SharedPreferences.Editor editor = PrimeCollegeApplication.getSharedPreference().edit();
        editor.putString(Constants.LOGIN_REPONSE, json);
        editor.apply();
    }

}
