package com.ingrails.primecollege.services;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ingrails.primecollege.R;

public class ServiceStartActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_start);
        Intent intent = new Intent(ServiceStartActivity.this, ServiceDemo.class);
        startService(intent);
    }
}
