package com.ingrails.primecollege.services;


import com.ingrails.primecollege.R;
import com.ingrails.primecollege.utils.PrimeCollegeApplication;

import java.io.InterruptedIOException;
import java.net.ConnectException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gokarna on 8/2/17.
 * custom error message for retrofit
 */

public class EventNepalCallback<T> implements Callback<T> {
    private String cause;


    @Override
    public void onResponse(Call<T> call, Response<T> response) {

    }

    @Override
    public void onFailure(Call<T> call, Throwable throwable) {
        if (throwable instanceof UnknownHostException) {
            cause = PrimeCollegeApplication.getAppContext().getString(R.string.no_internet_connection);
        } else if (throwable instanceof InterruptedIOException) {
            cause = PrimeCollegeApplication.getAppContext().getString(R.string.slow_internet_connection);
        } else if (throwable instanceof ConnectException) {
            cause = PrimeCollegeApplication.getAppContext().getString(R.string.cannot_connect_to_server);
        } else {
            cause = PrimeCollegeApplication.getAppContext().getString(R.string.unknown_error);
        }
    }

    public String getCause() {
        return cause;
    }
}
