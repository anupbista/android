package com.ingrails.primecollege.services;


import com.ingrails.primecollege.models.Login;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by gokarna on 3/15/17.
 * Login APi Service
 */

public interface SignUpApiService {
    @POST("register")
    @FormUrlEncoded
    Call<Login> registerUser(@Field("name") String name,
                             @Field("email") String email, @Field("password") String password,
                             @Field("password_confirmation") String confirmPassword,
                             @Field("dob") String dateOfBirth, @Field("gender") String gender);
}
