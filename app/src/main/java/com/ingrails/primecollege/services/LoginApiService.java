package com.ingrails.primecollege.services;


import com.ingrails.primecollege.models.Login;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by gokarna on 3/15/17.
 * Login APi Service
 */

public interface LoginApiService {
    @POST("login")
    @FormUrlEncoded
    Call<Login> requestLogin(@Field("email") String username,
                             @Field("password") String password, @Field("device_token") String deviceToken);
}
