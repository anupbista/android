package com.ingrails.primecollege.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.support.annotation.Nullable;

import java.io.IOException;

/**
 * Created by gokarna on 10/30/17.
 * Intent Service demo example
 */

public class IntentServiceDemo extends IntentService{
    private AssetFileDescriptor assetFileDescriptor;

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            assetFileDescriptor = getApplicationContext().getAssets().openFd("funnysong.mp3");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public IntentServiceDemo() {
        super("Intent Service Demo");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
