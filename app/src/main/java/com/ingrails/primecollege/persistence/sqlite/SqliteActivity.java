package com.ingrails.primecollege.persistence.sqlite;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.ingrails.primecollege.R;

import java.util.List;

public class SqliteActivity extends AppCompatActivity {
    private DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sqlite);
        addContact();
        getContactCount();
        getContactById();
        updateContact();
        deleteContact();
    }

    private void addContact() {
        databaseHandler = new DatabaseHandler(this);
        databaseHandler.addContact(new Contact("Ravi", "9100000000"));
        databaseHandler.addContact(new Contact("Srinivas", "9199999999"));
        databaseHandler.addContact(new Contact("Tommy", "9522222222"));
        databaseHandler.addContact(new Contact("Karthik", "9533333333"));
        getAllContacts();
    }

    private void getContactCount() {
        int count = databaseHandler.getContactsCount();
        Log.e("Count", String.valueOf(count));
    }

    private void getContactById() {
        Contact contact = databaseHandler.getContact();
        Log.e("id", String.valueOf(contact.getId()));
        Log.e("Name", contact.getName());
        Log.e("PhoneNumber", contact.getPhoneNumber());
    }

    private void updateContact() {
        Contact contact = new Contact();
        contact.setId(1);
        contact.setName("Ram Kumar Jha");
        contact.setPhoneNumber("9831234323");
        databaseHandler.updateContact(contact);
        getAllContacts();
    }

    private void deleteContact() {
        Contact contact = new Contact();
        contact.setId(2);
        databaseHandler.deleteContact(contact);
        getAllContacts();
    }

    private void getAllContacts() {
        List<Contact> contacts = databaseHandler.getAllContacts();
        for (Contact contact : contacts) {
            String log = "Id: " + contact.getId() + " ,Name: " + contact.getName() + " ,Phone: " + contact.getPhoneNumber();
            Log.e("Name: ", log);
        }
    }
}