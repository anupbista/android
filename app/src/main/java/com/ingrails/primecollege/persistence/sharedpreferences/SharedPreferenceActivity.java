package com.ingrails.primecollege.persistence.sharedpreferences;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.ingrails.primecollege.R;

public class SharedPreferenceActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preference_actvity);
        saveDataToSharedPreferences();
        getSavedSharedPreference();
    }

    @SuppressLint("ApplySharedPref")
    private void saveDataToSharedPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("name", "Shayam Basnet");
        editor.putInt("Age", 22);
        editor.commit();
    }

    private void getSavedSharedPreference() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String name = sharedPreferences.getString("name", null);
        int age = sharedPreferences.getInt("Age", 0);
        Log.e("Name", name);
        Log.e("Age", String.valueOf(age));
    }
}
