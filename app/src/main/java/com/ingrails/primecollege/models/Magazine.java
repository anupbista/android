package com.ingrails.primecollege.models;

/**
 * Created by gokarna on 11/18/17.
 */

public class Magazine {
    private String title;
    private int image;
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
