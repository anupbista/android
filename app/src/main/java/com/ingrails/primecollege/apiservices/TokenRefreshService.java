package com.ingrails.primecollege.apiservices;


import com.ingrails.primecollege.models.Login;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by gokarna on 7/2/17.
 */

interface TokenRefreshService {
    @GET("refresh-token")
    Call<Login> refreshToken();
}
