package com.ingrails.primecollege.apiservices;


import com.ingrails.primecollege.models.Login;
import com.ingrails.primecollege.utils.Utilities;

import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Route;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by gokarna on 7/2/17.
 * custom authenticator for api
 */

class ApiTokenAuthenticator implements Authenticator {
    @Override
    public Request authenticate(Route route, okhttp3.Response response) throws IOException {
        TokenRefreshService tokenRefreshService = ApiClient.getClient().create(TokenRefreshService.class);
        Call<Login> call = tokenRefreshService.refreshToken();
        Response<Login> tokenResponse = call.execute();
        if (tokenResponse.isSuccessful() && tokenResponse.body().getSuccess() == 1) {
            Login login = tokenResponse.body();
            Utilities.saveLoginResponse(login);
            return response.request().newBuilder()
                    .addHeader("Authorization", login.getUser().getAuthorization())
                    .addHeader("Accept", "Accept: application/x.school.v1+json")
                    .build();
        }
        return null;
    }
}
