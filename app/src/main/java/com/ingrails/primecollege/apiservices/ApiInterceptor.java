package com.ingrails.primecollege.apiservices;

import android.text.TextUtils;

import com.ingrails.primecollege.utils.Utilities;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by gokarna on 7/2/17.
 * custom interceptor for api
 */

class ApiInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        if (Utilities.getLoginResponse() == null || TextUtils.isEmpty(Utilities.getLoginResponse().getUser().getAuthorization())) {
            return chain.proceed(originalRequest);
        }
        Request request = originalRequest.newBuilder()
                .addHeader("Authorization", Utilities.getLoginResponse().getUser().getAuthorization())
                .addHeader("Accept", "Accept: application/x.school.v1+json")
                .build();
        return chain.proceed(request);
    }
}
