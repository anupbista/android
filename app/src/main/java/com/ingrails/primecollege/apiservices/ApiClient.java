package com.ingrails.primecollege.apiservices;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by gokarna on 4/30/17.
 * Api client for event nepal api
 */

public class ApiClient {
    private static final String BASE_URL = "https://nepalevents.com/api/v1/";

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.authenticator(new ApiTokenAuthenticator());
        OkHttpClient httpClient = builder.readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(new ApiInterceptor())
                .connectTimeout(60, TimeUnit.SECONDS).build();
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(httpClient).build();
        }
        return retrofit;
    }
}
