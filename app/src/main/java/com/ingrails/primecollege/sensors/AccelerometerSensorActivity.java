package com.ingrails.primecollege.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.ingrails.primecollege.R;

public class AccelerometerSensorActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager sensorManager;
    private Sensor sensor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accelerometer_sensor);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        int orientation = 0;
        float x = sensorEvent.values[0];
        float y = sensorEvent.values[1];
        if (x < 5 && x > -5 && y > 5) {
            orientation = 0;
        } else if (x < -5 && y < 5 && y > -5) {
            orientation = 90;
        } else if (x < 5 && x > -5 && y < -5) {
            orientation = 180;
        } else if (x > 5 && y < 5 && y > -5) {
            orientation = 270;
        }

        Log.e("Orientation", String.valueOf(orientation));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onPause() {
        sensorManager.unregisterListener(this, sensor);
        super.onPause();
    }
}
